package com.example.coelhoapp;

import android.os.AsyncTask;
import android.util.Log;

import com.example.coelhoapp.dto.LoginInputDTO;
import com.example.coelhoapp.dto.UsuarioDTO;
import com.google.gson.Gson;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

public class LoginTask extends AsyncTask<LoginInputDTO, Void, UsuarioDTO> {
    @Override
    protected UsuarioDTO doInBackground(LoginInputDTO... dados) {

        StringBuilder result = new StringBuilder();

        HttpsURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpsURLConnection) new URL("https://api.entregas.davesmartins.com.br/oauth/token").openConnection();

            // Sets the request method for the URL
            httpURLConnection.setRequestMethod("POST");

            httpURLConnection.setRequestProperty("Authorization","Basic YXBwOmFzZHNAQXNkIzIzMjQzRGFz");


            // Tells the URL that I am sending a POST request body
            httpURLConnection.setDoOutput(true);
            // Tells the URL that I want to read the response data
            httpURLConnection.setDoInput(true);

            // JSON object for the REST API
            String param = "grant_type=password&username="+dados[0].getUsername()+"&password="+dados[0].getPassword();
            byte[] paramBytes = param.getBytes(StandardCharsets.UTF_8);

            Log.i("parametro", param);

            // To write primitive Java data types to an output stream in a portable way
            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            // Writes out a byte to the underlying output stream of the data posted from .execute function
            wr.write(paramBytes);
            // Flushes the jsonParam to the output stream
            wr.flush();
            wr.close();

            // Representing the input stream
            InputStream in = new BufferedInputStream(httpURLConnection.getInputStream());

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            // reading the input stream / response from the url
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect(); //Fechando a conexão
            }
        }

        Log.i("TAG", result.toString());

        UsuarioDTO obj = new Gson().fromJson(result.toString(), UsuarioDTO.class);

        return obj;
    }
}
