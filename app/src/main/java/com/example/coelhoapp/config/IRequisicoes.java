package com.example.coelhoapp.config;

import com.example.coelhoapp.dto.CadastroDTO;
import com.example.coelhoapp.dto.EntregaDTO;
import com.example.coelhoapp.dto.EntregaInputDTO;
import com.example.coelhoapp.dto.LoginInputDTO;
import com.example.coelhoapp.dto.RotaDTO;
import com.example.coelhoapp.dto.UsuarioDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IRequisicoes {

    //Buscas
    @GET("entrega/rotas")
    public Call<List<RotaDTO>> getRotas(@Header("Authorization") String token);

    @GET("entrega/{id}")
    public Call<EntregaDTO> getEntrega(@Header("Authorization") String token, @Path("id") int idEntrega);

    @GET("entrega")
    public Call<List<EntregaDTO>> getMinhasEntregas(@Header("Authorization") String token);

    @GET("entrega/{b1}/{b2}/preco")
    public Call<RotaDTO> getFrete(@Header("Authorization") String token,
                                              @Path("b1") String bairroSaida, @Path("b2") String bairroDestino);

    @GET("usuarios")
    public Call<List<UsuarioDTO>> getUsuarios(@Header("Authorization") String token);

    @GET("entregas")
    public Call<List<EntregaDTO>> getEntregas(@Header("Authorization") String token);

    @GET("entrega/motoboy")
    public Call<List<EntregaDTO>> getEntregasAguardando(@Header("Authorization") String token);

    @GET("entrega/motoboy/my")
    public Call<List<EntregaDTO>> getEntregasAnteriores(@Header("Authorization") String token);


    //Salvar
    @POST("usuarios/cliente")
    public Call<CadastroDTO> saveCliente(@Header("Authorization") String token, @Body CadastroDTO usuario);

    @POST("entrega")
    public Call<EntregaDTO> saveEntrega(@Header("Authorization") String token, @Body EntregaInputDTO entrega);

    @POST("oauth/token")
    public Call<UsuarioDTO> execLogin(@Header("Authorization") String basic, @Body LoginInputDTO loginDto);


    //Alterar
    @PUT("entrega/motoboy/{id}/aceitar")
    public Call<EntregaDTO> aceitarEntrega(@Header("Authorization") String token, @Path("id") int idEntrega);

    @PUT("entrega/motoboy/{id}/finalizar")
    public Call<EntregaDTO> finalizarEntrega(@Header("Authorization") String token, @Path("id") int idEntrega);
}
