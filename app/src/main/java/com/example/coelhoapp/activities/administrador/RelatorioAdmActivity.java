package com.example.coelhoapp.activities.administrador;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.UsuarioDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

public class RelatorioAdmActivity extends AppCompatActivity {

    RecyclerView recyclerViewUsuarios;
    TextView textNumClientes, textNumEntregadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_adm);

        binding();

        UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");

        gerarRelatorio(userLogado);
    }

    private void gerarRelatorio(UsuarioDTO userLogado) {
        Retrofit r = new RetrofitConfig().getRetrofit();
        IRequisicoes req = r.create(IRequisicoes.class);

        Call<List<UsuarioDTO>> todosUsuarios = req.getUsuarios(userLogado.getToken());

        int contadorCliente=0, contadorEntregador=0;
        for(Object user : (List)todosUsuarios){
            UsuarioDTO usuario = (UsuarioDTO)user;

            if(usuario.getROLE().toLowerCase().equals("cliente"))
                contadorCliente++;
            if(usuario.getROLE().toLowerCase().equals("entregador"))
                contadorEntregador++;
        }

        exibirInformacoes(contadorCliente, contadorEntregador);
    }

    private void exibirInformacoes(int contadorCliente, int contadorEntregador) {
        textNumClientes.setText(Integer.toString(contadorCliente));
        textNumEntregadores.setText(Integer.toString(contadorEntregador));
    }

    private void binding() {
        recyclerViewUsuarios = findViewById(R.id.recyclerViewUsuarios);
        textNumClientes = findViewById(R.id.textNumClientes);
        textNumEntregadores = findViewById(R.id.textNumEntregadores);
    }
}