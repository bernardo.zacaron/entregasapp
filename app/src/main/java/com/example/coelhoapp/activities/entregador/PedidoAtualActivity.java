package com.example.coelhoapp.activities.entregador;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.EntregaDTO;
import com.example.coelhoapp.dto.UsuarioDTO;

import retrofit2.Retrofit;

public class PedidoAtualActivity extends AppCompatActivity {

    TextView textProduto, textSaida, textDestino, textObs;
    Button buttonConcluir, buttonAnteriores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_atual);

        binding();

        UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");
        EntregaDTO entregaAtual = (EntregaDTO) getIntent().getSerializableExtra("entrega");

        exibirInformacoes(entregaAtual);

        buttonConcluir.setOnClickListener(finalizarEntrega(entregaAtual, userLogado));
        buttonAnteriores.setOnClickListener(entregasAnteriores(userLogado));
    }

    private View.OnClickListener finalizarEntrega(EntregaDTO entregaAtual, UsuarioDTO userLogado) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Retrofit r = new RetrofitConfig().getRetrofit();
                IRequisicoes req = r.create(IRequisicoes.class);

                req.finalizarEntrega(userLogado.getToken(), entregaAtual.getIdentrega());

                Intent itn = new Intent(getApplicationContext(), PedidosEntregadorActivity.class);
                itn.putExtra("user", userLogado);

                startActivity(itn);
            }
        };
    }

    private void exibirInformacoes(EntregaDTO entrega) {
        textProduto.setText(entrega.getProduto());
        textSaida.setText(entrega.getBairroOrigem());
        textDestino.setText(entrega.getBairroDestino());
        if(!entrega.getObs().isEmpty())
            textObs.setText(entrega.getObs());
    }

    private View.OnClickListener entregasAnteriores(UsuarioDTO userLogado) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), PedidosAnterioresActivity.class);
                itn.putExtra("user", userLogado);

                startActivity(itn);
            }
        };
    }

    private void binding() {
        textProduto = findViewById(R.id.textProduto);
        textSaida = findViewById(R.id.textSaida);
        textDestino = findViewById(R.id.textDestino);
        buttonConcluir = findViewById(R.id.buttonConcluir);
        textObs = findViewById(R.id.textObsEntrega);
        buttonAnteriores = findViewById(R.id.buttonAnteriores);
    }
}