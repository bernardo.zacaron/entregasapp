package com.example.coelhoapp.activities.entregador;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.EntregaDTO;
import com.example.coelhoapp.dto.UsuarioDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

public class PedidosAnterioresActivity extends AppCompatActivity {

    TextView textGanhosEntregador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_anteriores);

        textGanhosEntregador = findViewById(R.id.textGanhosEntregador);

        UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");

        exibirGanhos(userLogado);
    }

    private void exibirGanhos(UsuarioDTO userLogado) {
        Retrofit r = new RetrofitConfig().getRetrofit();
        IRequisicoes req = r.create(IRequisicoes.class);

        double ganhosTotais = 0;

        Call<List<EntregaDTO>> minhasEntregas = req.getEntregasAnteriores(userLogado.getToken());

        for(EntregaDTO entrega : (List<EntregaDTO>)minhasEntregas){
            ganhosTotais += entrega.getPreco() * 0.2; //20% do valor das entregas
        }

        textGanhosEntregador.setText("R$" + Double.toString(ganhosTotais));

        //Exibir ao entregador as entregas e suas respectivas informações
    }
}