package com.example.coelhoapp.activities.entregador;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.EntregaDTO;
import com.example.coelhoapp.dto.UsuarioDTO;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

public class PedidosEntregadorActivity extends AppCompatActivity {

    Button buttonAceitarEntrega, buttonAnteriores;
    TextView textGanhosEntregador;

    Retrofit r = new RetrofitConfig().getRetrofit();
    IRequisicoes req = r.create(IRequisicoes.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos_entregador);

        binding();

        UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");

        listarEntregas(userLogado);

        buttonAnteriores.setOnClickListener(entregasAnteriores(userLogado));
    }

    private View.OnClickListener entregasAnteriores(UsuarioDTO userLogado) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), PedidoAtualActivity.class);
                itn.putExtra("user", userLogado);

                startActivity(itn);
            }
        };
    }

    private void listarEntregas(UsuarioDTO userLogado) {
        Call<List<EntregaDTO>> entregasPendentes = req.getEntregasAguardando(userLogado.getToken());

        exibirEntregas((List)entregasPendentes, userLogado);
    }

    private void exibirEntregas(List<EntregaDTO> entregasPendentes, UsuarioDTO userLogado) {
        //Exibir ao entregador as entregas que ainda não foram aceitas por outro entregador

        EntregaDTO entregaSelecionada = entregasPendentes.get(0);

        buttonAceitarEntrega.setOnClickListener(atribuirEntrega(userLogado, entregaSelecionada));
    }

    private View.OnClickListener atribuirEntrega(UsuarioDTO userLogado, EntregaDTO entregaSelecionada) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req.aceitarEntrega(userLogado.getToken(), entregaSelecionada.getIdentrega()); //Muda o status do pedido, fazendo com que não apareça para outros entregadores

                Intent itn = new Intent(getApplicationContext(), PedidoAtualActivity.class);
                itn.putExtra("entrega", entregaSelecionada);
                itn.putExtra("user", userLogado);

                startActivity(itn);
            }
        };
    }

    private void binding() {
        buttonAceitarEntrega = findViewById(R.id.buttonAceitarEntrega);
        buttonAnteriores = findViewById(R.id.buttonAnteriores);
    }
}