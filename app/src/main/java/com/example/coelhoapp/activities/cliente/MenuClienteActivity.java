package com.example.coelhoapp.activities.cliente;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.coelhoapp.R;

public class MenuClienteActivity extends AppCompatActivity {

    Button buttonNovaEntrega, buttonListaEntregas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_cliente);

        buttonNovaEntrega = findViewById(R.id.buttonNovaEntrega);
        buttonListaEntregas = findViewById(R.id.buttonListaEntregas);

        buttonNovaEntrega.setOnClickListener(novaEntrega());
        buttonListaEntregas.setOnClickListener(listaEntregas());
    }

    private View.OnClickListener listaEntregas() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), ListaClienteActivity.class);
                startActivity(itn);
            }
        };
    }

    private View.OnClickListener novaEntrega() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), PedidoClienteActivity.class);
                startActivity(itn);
            }
        };
    }
}