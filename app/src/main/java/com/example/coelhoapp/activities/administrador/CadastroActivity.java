package com.example.coelhoapp.activities.administrador;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.CadastroDTO;
import com.example.coelhoapp.dto.UsuarioDTO;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Retrofit;

public class CadastroActivity extends AppCompatActivity {

    TextInputEditText textNome, textEmail, textCpf, textEndereco, textIdade, textTelefone, textSenhaCadastro;
    Button buttonCadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        binding();

        UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");

        buttonCadastro.setOnClickListener(cadastrarCliente(userLogado));
    }

    private View.OnClickListener cadastrarCliente(UsuarioDTO userLogado) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Retrofit r = new RetrofitConfig().getRetrofit();
                IRequisicoes req = r.create(IRequisicoes.class);

                CadastroDTO novoCad = new CadastroDTO(textNome.getText().toString(), textEmail.getText().toString(),
                        textCpf.getText().toString(), textEndereco.getText().toString(), textIdade.getText().toString(),
                        textTelefone.getText().toString(), textSenhaCadastro.getText().toString());

                req.saveCliente(userLogado.getToken(), novoCad);
            }
        };
    }

    private void binding() {
        textNome = findViewById(R.id.textNome);
        textEmail = findViewById(R.id.textEmail);
        textCpf = findViewById(R.id.textCpf);
        textEndereco = findViewById(R.id.textEndereco);
        textIdade = findViewById(R.id.textIdade);
        textTelefone = findViewById(R.id.textTelefone);
        textSenhaCadastro = findViewById(R.id.textSenhaCadastro);
        buttonCadastro = findViewById(R.id.buttonCadastro);
    }
}