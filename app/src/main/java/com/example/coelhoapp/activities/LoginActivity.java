package com.example.coelhoapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.coelhoapp.R;
import com.example.coelhoapp.activities.administrador.RelatorioAdmActivity;
import com.example.coelhoapp.activities.cliente.PedidoClienteActivity;
import com.example.coelhoapp.activities.entregador.PedidosEntregadorActivity;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.LoginInputDTO;
import com.example.coelhoapp.dto.UsuarioDTO;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {
    TextView textCadastro;
    TextInputEditText textLogin, textSenha;
    Button buttonEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        binding();

        boolean ehCliente = getIntent().getExtras().getBoolean("ehCliente");

        if(!ehCliente)
            textCadastro.setVisibility(View.INVISIBLE); //Esconder botão de cadastro caso seja adm

        buttonEntrar.setOnClickListener(clickLogin());
        textCadastro.setOnClickListener(abrirCadastro());

    }

    private View.OnClickListener abrirCadastro() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Abrir tela de cadastro
            }
        };
    }

    private View.OnClickListener clickLogin() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = textLogin.getText().toString();
                String senha = textSenha.getText().toString();

                Retrofit r = new RetrofitConfig().getRetrofit();

                IRequisicoes req = r.create(IRequisicoes.class);

                LoginInputDTO input = new LoginInputDTO(login, senha, "password");

                Call<UsuarioDTO> user = req.execLogin("Basic YXBwOmFzZHNAQXNkIzIzMjQzRGFz", input);
                user.enqueue(new Callback<UsuarioDTO>() {
                    @Override
                    public void onResponse(Call<UsuarioDTO> call, Response<UsuarioDTO> response) {
                        Log.i("login", response.body().getNome());
                    }

                    @Override
                    public void onFailure(Call<UsuarioDTO> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "falha", Toast.LENGTH_LONG).show();
                    }
                });
                UsuarioDTO usuario = null;

                try {
                    usuario = user.execute().body();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (user != null) {
                    if(usuario.getROLE().toLowerCase().equals("cliente")){
                        Intent itn = new Intent(getApplicationContext(), PedidoClienteActivity.class);

                        itn.putExtra("user", (UsuarioDTO)user);
                        startActivity(itn);
                    }else if(usuario.getROLE().toLowerCase().equals("entregador")){
                        Intent itn = new Intent(getApplicationContext(), PedidosEntregadorActivity.class);

                        itn.putExtra("user", (UsuarioDTO)user);
                        startActivity(itn);
                    }else{
                        Intent itn = new Intent(getApplicationContext(), RelatorioAdmActivity.class);

                        itn.putExtra("user", (UsuarioDTO)user);
                        startActivity(itn);
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Login ou senha incorreta", Toast.LENGTH_SHORT).show();

                    textLogin.setText("");
                    textSenha.setText("");
                    textLogin.requestFocus();
                }
                finish();
            }
        };
    }

    private void binding() {
        textCadastro = findViewById(R.id.textCadastro);
        textLogin = findViewById(R.id.textLogin);
        textSenha = findViewById(R.id.textSenha);
        buttonEntrar = findViewById(R.id.buttonEntrar);
    }
}