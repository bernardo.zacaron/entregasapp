package com.example.coelhoapp.activities.cliente;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.EntregaInputDTO;
import com.example.coelhoapp.dto.RotaDTO;
import com.example.coelhoapp.dto.UsuarioDTO;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Retrofit;

public class PedidoClienteActivity extends AppCompatActivity {

    TextInputEditText textItem, textBairroSaida, textBairroEntrega, textObs;
    Button buttonCalcular, buttonConfirmar;
    TextView textBairros, textPreco, textTitulo, textCusto;

    Retrofit r = new RetrofitConfig().getRetrofit();
    IRequisicoes req = r.create(IRequisicoes.class);

    UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido_cliente);

        binding();

        buttonCalcular.setOnClickListener(calcularEntrega());
    }

    private View.OnClickListener calcularEntrega() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(textItem.getText().toString().isEmpty() || textBairroSaida.getText().toString().isEmpty() || textBairroEntrega.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(), "Campo obrigatório não preenchido!", Toast.LENGTH_SHORT).show();
                    textItem.setText("");
                    textBairroSaida.setText("");
                    textBairroEntrega.setText("");
                    textItem.requestFocus();
                    return; //Validações de preenchimento
                }

                EntregaInputDTO entrega = new EntregaInputDTO(textItem.getText().toString(), textBairroSaida.getText().toString(), textBairroEntrega.getText().toString(), textObs.getText().toString());

                exibirEConfirmar(entrega);
                //req.saveEntrega(userLogado.getToken(), entrega);
            }
        };
    }

    private void exibirEConfirmar(EntregaInputDTO entrega) { //Mostra as informações da entrega para o usuario finalizar seu pedido
        textTitulo.setVisibility(View.VISIBLE);
        textBairros.setVisibility(View.VISIBLE);
        textCusto.setVisibility(View.VISIBLE);
        textPreco.setVisibility(View.VISIBLE);
        buttonConfirmar.setVisibility(View.VISIBLE);

        double frete = calcularFrete(entrega.getBairroOrigem(), entrega.getBairroDestino());

        textBairros.setText("Seu pedido vai de "+ entrega.getBairroOrigem() +" até "+ entrega.getBairroDestino());
        textPreco.setText("R$"+Double.toString(frete));

        buttonConfirmar.setOnClickListener(confirmarEntrega(entrega));
    }

    private View.OnClickListener confirmarEntrega(EntregaInputDTO entrega) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                req.saveEntrega(userLogado.getToken(), entrega);

                Toast.makeText(getApplicationContext(), "Entrega salva. Seu pedido logo chegará ao destino!", Toast.LENGTH_LONG).show();

                Intent itn = new Intent(getApplicationContext(), MenuClienteActivity.class);
                startActivity(itn);
            }
        };
    }

    private double calcularFrete(String bairro1, String bairro2) {
        RotaDTO rota = (RotaDTO)req.getFrete(userLogado.getToken(), bairro1, bairro2);

        return rota.getPrecoBase();
    }

    private void binding() {
        textItem = findViewById(R.id.textItem);
        textBairroSaida = findViewById(R.id.textBairroSaida);
        textBairroEntrega = findViewById(R.id.textBairroEntrega);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        textObs = findViewById(R.id.textObs);

        buttonConfirmar = findViewById(R.id.buttonConfirmarInv);
        textBairros = findViewById(R.id.textBairrosInv);
        textPreco = findViewById(R.id.textPrecoInv);
        textTitulo = findViewById(R.id.textTituloInv);
        textCusto = findViewById(R.id.textCustoInv);
    }
}