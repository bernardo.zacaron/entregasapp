package com.example.coelhoapp.activities.cliente;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;
import com.example.coelhoapp.config.RetrofitConfig;
import com.example.coelhoapp.dto.EntregaDTO;
import com.example.coelhoapp.dto.UsuarioDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

public class ListaClienteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cliente);

        UsuarioDTO userLogado = (UsuarioDTO) getIntent().getSerializableExtra("user");

        gerarLista(userLogado);
    }

    private void gerarLista(UsuarioDTO userLogado) {
        Retrofit r = new RetrofitConfig().getRetrofit();
        IRequisicoes req = r.create(IRequisicoes.class);

        Call<List<EntregaDTO>> entregas = req.getMinhasEntregas(userLogado.getToken());

        exibirLista(entregas);
    }

    private void exibirLista(Call<List<EntregaDTO>> entregas) {
        //Exibir ao user suas entregas, informando o status e o produto de cada entrega
    }
}