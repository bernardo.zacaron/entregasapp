package com.example.coelhoapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.coelhoapp.R;
import com.example.coelhoapp.config.IRequisicoes;

public class MainActivity extends AppCompatActivity {
    Button buttonLoginCliente, buttonLoginEntregador, buttonLoginAdm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonLoginCliente = findViewById(R.id.buttonLoginCliente);
        buttonLoginEntregador = findViewById(R.id.buttonLoginEntregador);
        buttonLoginAdm = findViewById(R.id.buttonLoginAdm);

        buttonLoginCliente.setOnClickListener(chamarLogin(true));
        buttonLoginEntregador.setOnClickListener(chamarLogin(false));
        buttonLoginAdm.setOnClickListener(chamarLogin(false));
    }

    private View.OnClickListener chamarLogin(boolean ehCliente) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent itn = new Intent(getApplicationContext(), LoginActivity.class);

                itn.putExtra("ehCliente", ehCliente);
                startActivity(itn);
            }
        };
    }
}