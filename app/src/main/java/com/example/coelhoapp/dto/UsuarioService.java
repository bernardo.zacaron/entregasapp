package com.example.coelhoapp.dto;

import com.example.coelhoapp.LoginTask;

public class UsuarioService {
    public UsuarioDTO logar(String login, String senha){
        LoginTask task = new LoginTask();

        LoginInputDTO log = new LoginInputDTO(login, senha, "password");

        try {
            return task.execute(log).get();
        } catch (Exception e) {
            return null;
        }
    }
}

