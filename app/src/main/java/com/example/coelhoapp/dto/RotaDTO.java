package com.example.coelhoapp.dto;

public class RotaDTO {
    private String bairro1, bairro2;
    private int km;
    private double precoBase;

    public String getBairro1() {
        return bairro1;
    }

    public void setBairro1(String bairro1) {
        this.bairro1 = bairro1;
    }

    public String getBairro2() {
        return bairro2;
    }

    public void setBairro2(String bairro2) {
        this.bairro2 = bairro2;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public double getPrecoBase() {
        return precoBase;
    }

    public void setPrecoBase(double precoBase) {
        this.precoBase = precoBase;
    }

    public RotaDTO(String bairro1, String bairro2, int km, double precoBase) {
        this.bairro1 = bairro1;
        this.bairro2 = bairro2;
        this.km = km;
        this.precoBase = precoBase;
    }

    public RotaDTO() {
    }
}
