package com.example.coelhoapp.dto;

public class EntregaInputDTO {
    private String produto, bairroOrigem, bairroDestino, obs;

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public String getBairroOrigem() {
        return bairroOrigem;
    }

    public void setBairroOrigem(String bairroOrigem) {
        this.bairroOrigem = bairroOrigem;
    }

    public String getBairroDestino() {
        return bairroDestino;
    }

    public void setBairroDestino(String bairroDestino) {
        this.bairroDestino = bairroDestino;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public EntregaInputDTO(String produto, String bairroOrigem, String bairroDestino, String obs) {
        this.produto = produto;
        this.bairroOrigem = bairroOrigem;
        this.bairroDestino = bairroDestino;
        this.obs = obs;
    }

    public EntregaInputDTO() {
    }
}
